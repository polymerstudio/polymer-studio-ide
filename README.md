# PolymerStudio IDE

![PolymerStudio](https://www.polymer-project.org/images/logos/p-logo.png)


# Polymer Development made easy! Ready for war!

(https://itunes.apple.com/us/app/polymerstudio)
## Download on the App Store 

(http://www.stickpng.com/assets/images/5847e95fcef1014c0b5e4822.png)
## Download on the Windows Store

## Table of Contents

- [Introduction](#introduction)
- [Features](#features)
- [Feedback](#feedback)
- [Build Process](#build-process)
- [Team](#team-)
- [Sponsors](#sponsors-)


## Introduction

Build polymer powered applications ...

**Available for OSX, Windows and Linux.**


## Features

Features included in PolymerStudio

* Project Explorer
* File Tree Eplorer
* WebComponents Explorer

(http://i.imgur.com/IkSnFRL.png)
(http://i.imgur.com/0iorG20.png)



## Feedback

Feel free to send us feedback on [Twitter](https://twitter.com/polymerstudio) or [file an issue](https://bitbucket/issues??). Feature requests are always welcome. If you wish to contribute, please get in touch with our team!



## Contributors

This project follows the [all-contributors](https://github.com/kentcdodds/all-contributors) specification.



## Build Process

- Clone or download the repo
- `git clone git@bitbucket.org:polymerstudio/polymer-studio-ide.git`
- `npm install` to install dependencies
- `npm run start`



**Development Keys**
TODO: how this thing works 


# Team
