/// <reference path='../../../node_modules/electron/electron.d.ts' />

const electron = require("electron");
const path = require("path");
const url = require("url");

class ElectronMain {
  init: any = (function init() {
    electron.app.on("ready", () => {
      let {
        width,
        height
      } = require("electron").screen.getPrimaryDisplay().size;
      let options: Electron.BrowserWindowConstructorOptions = {
        webPreferences: {
          experimentalFeatures: true,
          experimentalCanvasFeatures: true
        },
        //titleBarStyle: "hidden",
        resizable: true,
        width: width,
        height: height,
        //show: false,
        frame: false
      };
      const mainWindow = new electron.BrowserWindow(options);

      if (electron.app.dock) {
        electron.app.dock.setIcon(electron.nativeImage.createFromPath("./assets/icons/icon.png"));
      } else {
        mainWindow.setIcon(
          electron.nativeImage.createFromPath("./assets/icons/icon.png")
        );
      }

      mainWindow.loadURL(
        url.format({
          pathname: path.join(__dirname, "./index.html"),
          protocol: "file:",
          slashes: true
        })
      );

      mainWindow.webContents.on("did-finish-load", () => {
        mainWindow.show();
        mainWindow.focus();
        // Open the DevTools.
        mainWindow.webContents.openDevTools();
      });

      //app.on("open-file", (_event, file) => browserWindow.webContents.send("change-working-directory", file));
    });

    electron.app.on("window-all-closed", () => electron.app.quit());

    electron.ipcMain.on("quit", electron.app.quit);
  })();
}

new ElectronMain();
