declare module "*.html" {
  const content: string;
  export default content;
}

declare module "*.css" {
  interface IClassNames {
    [className: string]: string;
  }
  const classNames: IClassNames;
  export = classNames;
}

declare module 'node-pty' {
	export function fork(file: string, args: string[], options: any): Terminal;
	export function spawn(file: string, args: string[], options: any): Terminal;
	export function createTerminal(file: string, args: string[], options: any): Terminal;

	export interface Terminal {
		pid: number;

		/**
		 * The title of the active process.
 		 */
		process: string;

		on(event: string, callback: (data: any) => void): void;

		resize(columns: number, rows: number): void;

		write(data: string): void;

		kill(): void;
	}
}