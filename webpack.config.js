const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CleanWebpackPlugin = require("clean-webpack-plugin");
const CommonsChunkPlugin = require("webpack/lib/optimize/CommonsChunkPlugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");

const webpack = require("webpack");

module.exports = {
  target: "electron",
  entry: {
    app: "./src/index"
  },
  output: {
    filename: "[name].js",
    sourceMapFilename: "[name].js.map",
    path: path.resolve(__dirname, "dist")
  },
  module: {
    rules: [
      {
        test: /\.(html)$/,
        use: {
          loader: "html-loader"
        }
      },
      {
        test: /\.ts?$/,
        use: "ts-loader",
        exclude: [path.resolve(__dirname, "/node_modules/")]
      }
    ]
  },
  resolve: {
    extensions: [".ts", ".js", ".html"]
  },
  plugins: [
    //new CleanWebpackPlugin(['dist'], { root: path.resolve(__dirname) }),
    new HtmlWebpackPlugin({
      template: "./index.html",
      inject: false
    }),
    // copy custom static assets
    new CopyWebpackPlugin([
      {
        from: path.resolve(__dirname, "assets"),
        to: "assets",
        ignore: [".*"]
      },
      {
        from: path.resolve(
          __dirname,
          "./node_modules/@webcomponents/webcomponentsjs/webcomponents-loader.js"
        )
      }
    ]),
    // get around with stupid warning
    new webpack.IgnorePlugin(/vertx/)
  ],
  externals: (ctx, req, done) =>
    /^node-pty$/.test(req) ? done(null, `commonjs ${req}`) : done()
};
